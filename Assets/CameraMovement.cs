﻿using UnityEngine;
using System.Collections;

namespace gc{
	[RequireComponent (typeof (Camera))]
	public class CameraMovement : MonoBehaviour {

		public static CameraMovement Instance { get; private set; }


		[Range(0f,1f)]
		public float LoSPercent;

		public float MoveSpeed;
		Coroutine moveCorotouine;
		float xPos;
		Camera cam;
		Vector3 targetPos;

		void Awake() {
			Instance = this;
			targetPos = transform.position;
		}
		void Start () 
		{
			EventManager.Instance.Register<CompletedPassEvent>(OnCompletedPass);
			xPos = transform.position.x;
			cam = GetComponent<Camera>();
		}

		IEnumerator MoveToSpot(float newSpot)
		{
			float start = transform.position.y;
			targetPos = new Vector3(xPos, newSpot,-10f);
			for(float t=0; t<1.0f; t+=Time.deltaTime*MoveSpeed)
			{
				float newY = Mathf.Lerp(start,newSpot,t);
				transform.position = new Vector3(xPos, newY, -10f);
				yield return null;
			}
			transform.position = new Vector3(xPos, newSpot, -10f);
		}

		public Bounds CamBoundsAtCurrentPosition()
		{
			return CamBoundsAtPosition(transform.position);
		}

		Bounds CamBoundsAtPosition(Vector3 pos)
		{
			float screenAspect = (float)Screen.width / (float)Screen.height;
			float cameraHeight = cam.orthographicSize*2;
			Bounds bounds = new Bounds(
				pos,
				new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
			return bounds;
		}

		public float CameraYAsPercent(float perc)
		{
			Bounds camBounds = CamBoundsAtPosition(targetPos);
			float minPos = camBounds.min.y;
			float height = camBounds.extents.y*2f;
			return minPos+(height*perc);
		}

		public float CamXAsPercent(float perc)
		{
			Bounds camBounds = CamBoundsAtPosition(targetPos);
			float minPos = camBounds.min.x;
			float width = camBounds.extents.x*2f;
			return minPos+(width*perc);
		}

		void OnCompletedPass(Event e)
		{
			//find new line of scrimmage point
			CompletedPassEvent cPE = (CompletedPassEvent)e;
			float losPos = cPE.yPosition;
			//find camera destination
			float destPos = CameraDestination(losPos);
			//Debug.Log("DP: " + destPos);
			if(destPos>transform.position.y)
			{
				//move to destination
				if(moveCorotouine!=null)
				{
					StopCoroutine(moveCorotouine);
				}
				moveCorotouine = StartCoroutine(MoveToSpot(destPos));
			}
		}

		float CameraDestination(float losPosition)
		{
			Bounds camBounds = CamBoundsAtPosition(new Vector3(transform.position.x, losPosition, transform.position.z));
			float minPos = camBounds.min.y;
			float height = camBounds.extents.y*2f;
			return minPos + ((1-LoSPercent)*height);
		}

		void OnDestroy()
		{
			EventManager.Instance.Unregister<CompletedPassEvent>(OnCompletedPass);
		}
	}
}
