﻿using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

	public class Enemy : MonoBehaviour {
	

		protected AudioClip appearSound;
		protected AudioClip deadSound;
		protected EnemyManager enemyManager;

		protected virtual void Awake() {
			appearSound = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_appear");
			AudioSource.PlayClipAtPoint(appearSound, Camera.main.transform.position);
		}

		protected virtual void Start(){
			enemyManager = FindObjectOfType<EnemyManager>();
		}

		void OnCollisionEnter2D(Collision2D coll) 
		{
			if (coll.gameObject.CompareTag("PLAYER")) {
				coll.gameObject.GetComponent<PlayerMovement>().HitByEnemy();
				MyDestroy();
			}
		}

		void OnDestroy() {
		}

		public void MyDestroy(){
			AudioSource.PlayClipAtPoint(deadSound, Camera.main.transform.position);
			enemyManager.DestroyEnemy(this);
			//Destroy(gameObject);
		}
	}

}

