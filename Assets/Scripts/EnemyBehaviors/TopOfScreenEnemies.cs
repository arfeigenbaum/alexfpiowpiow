﻿using UnityEngine;
using System.Collections;

namespace gc{
	public class TopOfScreenEnemies : MonoBehaviour {

		float spawnTimer;
		public float spawnTimerStart;
		public int startingEnemies;
		float delayedStart = 1f;
		protected void Start ()
		{
			spawnTimer=spawnTimerStart+delayedStart;
			Invoke("DelayedStart", delayedStart);
		}

		void DelayedStart()
		{
			for(int i=0; i<startingEnemies; i++)
			{
				SpawnRandomEnemyAtTopOfScreen();
			}
		}

		protected Vector3 RandomPointAtTopOfScreen()
		{
			Bounds bounds = CameraMovement.Instance.CamBoundsAtCurrentPosition();
			float x = Random.Range(bounds.min.x, bounds.max.x);
			float y = bounds.max.y;
			return new Vector3(x,y,0f);
		}

		protected void Update()
		{
			RunTimer();
			if(spawnTimer<0)
			{
				SpawnRandomEnemyAtTopOfScreen();
				spawnTimer=spawnTimerStart;
			}
		}

		void RunTimer()
		{
			spawnTimer -= Time.deltaTime;
		}

		void SpawnRandomEnemyAtTopOfScreen()
		{
			EnemyManager.Instance.SpawnEnemyRandom(RandomPointAtTopOfScreen());
		}

	}
}
