﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace gc{
	public class BossEnemy : WaveManager 
	{
		enum BossState{
			regular,
			spawningEnemies,
			alwaysMoving
		}

		TaskManager tm;
		float scaleStart =.01f;
		float scaleTarget = 2f;
		float scaleSpeed = 1f;
		int hits = 0;
		Bounds levelBounds;
		Vector3 moveDestination;
		float moveSpeed = 30f;

		RepeatActionOverTimeTask moveTask;

		BossState bossState = BossState.regular;

		protected override void Start()
		{
			base.Start();
			transform.localScale = Vector3.one*scaleStart;
			//have your own taskmanager
			tm = new TaskManager();
			RepeatActionOverTimeTask scale = new RepeatActionOverTimeTask(ScaleUp, TimeToDoAction((scaleTarget-scaleStart), scaleSpeed));
			tm.AddTask(scale);
			levelBounds = LevelBounds();
		}

		Bounds LevelBounds()
		{
			Camera cam = Camera.main;
			float height = 2f * cam.orthographicSize;
			float width = height * cam.aspect;
			Vector3 center = new Vector3(width/2f, height/2f, 0f);
			return new Bounds(center, center);
		}

		protected override void OnWaveEnded(Event e)
		{
			base.OnWaveEnded(e);
		}

		protected override void OnNeedsNewWave(Event e)
		{
			base.OnNeedsNewWave(e);
		}

		protected override void OnDestroy()
		{
			EventManager.Instance.Unregister<NeedsNewWaveEvent>(OnNeedsNewWave);
			EventManager.Instance.Unregister<WaveEndedEvent>(OnWaveEnded);
		}

		void Update()
		{
			tm.Update();
		}
		void OnTriggerEnter2D(Collider2D col)
		{
			//if you hit the ball
			//add the number of hits, and check to change state
			if(col.tag == "BALL" && Ball.Instance.ballState == Ball.BallState.inAir){
				EventManager.Instance.Fire(new ScoredGoalEvent());
				hits++;
				//change state at these points
				if(hits>=2)
				{
					bossState = BossState.spawningEnemies;
				}
				if(hits>=3)
				{
					bossState = BossState.alwaysMoving;
				}
				//if you ahve an old move task
				if(moveTask!=null)
				{
					moveTask.Abort();
				}
				MoveAfterHit();
				//EventManager
				//if you aren't in regular state make a new enemy after you get hit
				if(bossState != BossState.regular)
				{
					SpawnExtraEnemies();
				}

			}
		}

		void SpawnExtraEnemies()
		{
			//wait half a second so you don't spawn on top of the ball
			WaitTask wait = new WaitTask(500f);
			//then make an enemy
			ActionTask act = new ActionTask(SpawnEnemyAtPosition);
			wait.Then(act);
			tm.AddTask(wait);
		}

		void SpawnEnemyAtPosition()
		{
			//spawn a new enemy in the center of the goal
			EnemyManager.Instance.SpawnEnemyRandom(transform.position);
		}

		void Move()
		{
			//move toward your new destination, called through repeating task
			if(Vector3.Distance(transform.position, moveDestination) < moveSpeed*Time.deltaTime)
			{
				transform.position = moveDestination;
			}
			else
			{
				Vector3 direction = moveDestination-transform.position;
				direction = direction.normalized;
				Vector3 moveAmt = direction*moveSpeed*Time.deltaTime;
				transform.position+=moveAmt;
			}
		}

		void MoveAfterHit()
		{

			//get a new spot
			Vector3 newSpot = RandomSpotAtLeastSomeDistanceAway();
			moveDestination = newSpot;
			//then add the move task to move there
			moveTask = new RepeatActionOverTimeTask(Move, TimeToDoAction(Vector3.Distance(newSpot, transform.position), moveSpeed));
			if(bossState == BossState.alwaysMoving)
			{
				ActionTask moveAgain = new ActionTask(MoveAfterHit);
				moveTask.Then(moveAgain);
			}
			tm.AddTask(moveTask);
		}

		//returns a new spot
		Vector3 RandomSpotAtLeastSomeDistanceAway()
		{
			//minimum distance
			float minMoveDistance = 20f;
			Vector3 newPos = transform.position;
			//get a new point until you are over the minimum distance
			while(Vector3.Distance(newPos, transform.position) < minMoveDistance){
				float x = Random.Range(levelBounds.min.x, levelBounds.max.x);
				float y = Random.Range(levelBounds.min.y, levelBounds.max.y);
				newPos = new Vector3(x, y, 0f);
			}
			return newPos;
		}
		//increase scale
		void ScaleUp()
		{
			transform.localScale+=Vector3.one*scaleSpeed*Time.deltaTime;
		}
		//calculate the length of time to do a task over and over again based on distance and speed
		float TimeToDoAction(float distance, float actionSpeed)
		{
			return distance/actionSpeed*1000f;
		}
	}
}
