﻿using UnityEngine;
using System.Collections;
namespace gc{
	public class DashEnemySound : Enemy {

		protected override void Awake ()
		{
			base.Awake ();
			deadSound=(AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_destroyed_2");
		}
	}
}
