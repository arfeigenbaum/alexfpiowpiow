﻿using UnityEngine;
using System.Collections;
namespace gc{
	public class ZoneEnemySound : Enemy {

		protected override void Awake ()
		{
			base.Awake ();
			deadSound=(AudioClip)ResourceLoader.Instance.GetResource("Sounds/enemy_destroyed_3");
		}
	}
}
