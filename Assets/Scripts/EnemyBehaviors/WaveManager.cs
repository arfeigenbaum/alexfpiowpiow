﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum EnemyType
{
	Zone,
	Blitz,
	Dash,
	State
};
	
namespace gc{
	public class WaveManager : MonoBehaviour  {

		public List<WaveOfEnemies> waves = new List<WaveOfEnemies>();
		protected int waveNumber = 0;


		virtual protected void Start()
		{
			EventManager.Instance.Register<NeedsNewWaveEvent>(OnNeedsNewWave);
			EventManager.Instance.Register<WaveEndedEvent>(OnWaveEnded);
		}

		protected virtual void OnWaveEnded(Event e)
		{
			waveNumber++;
			if(waveNumber>waves.Count)
			{
				//or end game?
				waveNumber=0;
			}
		}

		protected virtual void OnNeedsNewWave(Event e)
		{
			EnemyManager.Instance.SpawnWave(waves[waveNumber]);
		}

		protected virtual void OnDestroy()
		{
			EventManager.Instance.Unregister<NeedsNewWaveEvent>(OnNeedsNewWave);
			EventManager.Instance.Unregister<WaveEndedEvent>(OnWaveEnded);
		}

		[System.Serializable]
		public class WaveOfEnemies
		{
			public List<EnemyInformation> enemiesThisWave = new List<EnemyInformation>();
			public float waveTime;
		}

		[System.Serializable]
		public struct EnemyInformation
		{
			public Vector3 spawnLocation;
			public EnemyType myType;
		}
	}
}
