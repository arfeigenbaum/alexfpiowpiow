﻿
using System;
using System.Diagnostics;
using UnityEngine;

namespace gc{
	public class WaitTask : Task
	{
		// Get the timestamp in floating point milliseconds from the Unix epoch
		private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1);
		private static double GetTimestamp()
		{
			return (DateTime.UtcNow - UnixEpoch).TotalMilliseconds;
		}
		protected double _duration;
		private double _startTime;
		//QUESTIONS: Why do I need an empty constructor
		//Why can't I override a read only? Why is this a readonly to begin with?
		public WaitTask()
		{
			
		}
		public WaitTask(double duration)
		{
			this._duration = duration;
		}
		protected override void Init()
		{
			_startTime = GetTimestamp();
		}
		internal override void Update()
		{
			base.Update();
			_duration -= Time.deltaTime*1000f;
			if (_duration<=0)
			{
				SetStatus(TaskStatus.Success);
			}
		}
	}
	//do an action
	public class ActionTask: Task
	{
		private readonly Action _action;
		public ActionTask(Action action)
		{
			_action = action;
		}
		protected override void Init()
		{
			SetStatus(TaskStatus.Success);
			_action();
		}
	}
	//do an action over and over every frame for x number of frames
	public class RepeatActionOverTimeTask: WaitTask
	{
		private readonly Action _action;
		public RepeatActionOverTimeTask(Action action, double duration)
		{
			this._duration = duration;
			this._action = action;
		}
		internal override void Update()
		{
			base.Update();
			_action();
		}
	}

}
