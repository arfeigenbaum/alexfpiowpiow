﻿using System;
using System.Diagnostics;

namespace gc
{
	
	public abstract class Task
	{
		protected Task()
		{
			Status = TaskStatus.Detached;
		}

		#region Status

		public enum TaskStatus : byte
		{
			Detached, // Task has not been attached to a TaskManager 
			Pending, // Task has not been initialized
			Working, // Task has been initialized and has started receiving updates
			Success, // Task completed successfully
			Fail, // Task completed unsucessfully
			Aborted // Task was aborted
		}

		public TaskStatus Status { get; private set; }

//		// Convenience status checking
		public bool IsDetached { get { return Status == TaskStatus.Detached;} }
		public bool IsAttached { get { return Status != TaskStatus.Detached;} }
		public bool IsPending { get { return Status == TaskStatus.Pending; } }
		public bool IsWorking { get { return Status == TaskStatus.Working; } }
		public bool IsSuccessful { get { return Status == TaskStatus.Success;} }
		public bool IsFailed { get { return Status == TaskStatus.Fail; } }
		public bool IsAborted { get { return Status == TaskStatus.Aborted; }}
		public bool IsFinished { get { return (Status == TaskStatus.Fail || 
			Status == TaskStatus.Success || 
			Status == TaskStatus.Aborted); } }
		
		// Convenience method for external classes to abort the task
		public void Abort()
		{
			SetStatus(TaskStatus.Aborted);
		}

		// Changes the status of the task. Do not call this method more than once
		// per update of the containing task manager
		internal void SetStatus(TaskStatus newStatus)
		{
			if (Status == newStatus) return;

			Status = newStatus;

			switch (newStatus)
			{
			case TaskStatus.Working:
				// Initialize the task when the Task first starts
				// It's important to separate initialization from
				// the constructor, since tasks may not start
				// running until long after they've been constructed
				Init();
				break;

				// Success/Aborted/Failed are the completed states of a task.
				// Subclasses are notified when entering one of these states
				// and are given the opportunity to do any clean up
			case TaskStatus.Success:
				OnSuccess();
				CleanUp();
				break;

			case TaskStatus.Aborted:
				OnAbort();
				CleanUp();
				break;

			case TaskStatus.Fail:
				OnFail();
				CleanUp();
				break;

				// These are "internal" states that are mostly relevant for
				// the task manager
			case TaskStatus.Detached:
			case TaskStatus.Pending:
				break;
			default:
				break;
				//throw new ArgumentOutOfRangeException(nameof(newStatus), newStatus, null);
			}
		}

		// Subclasses can override these to respond to status changes
		protected virtual void OnAbort()
		{
		}

		protected virtual void OnSuccess()
		{
		}

		protected virtual void OnFail()
		{
		}

		#endregion

		#region Lifecycle

		// Override this to handle initialization of the task.
		// This is called when the task enters the Working status
		protected virtual void Init()
		{
		}

		internal virtual void Update()
		{
			if(Game.Instance.isPaused)
			{
				return;
			}
		}

		// This is called when the tasks completes (i.e. is aborted,
		// fails, or succeeds). It is called after the status change
		// handlers are called
		protected virtual void CleanUp()
		{
		}

		#endregion

		#region Sequencing

		public Task NextTask { get; private set; }

		// Sets a task to be automatically attached when this one completes successfully
		// NOTE: if a task is aborted or fails, its next task will not be queued
		// NOTE: **DO NOT** assign attached tasks with this method.
		public Task Then(Task task)
		{
			Debug.Assert(!task.IsAttached);
			NextTask = task;
			return task;
		}

		// A helper method for easily concatenating actions
		public Task Then(Action action)
		{
			var task = new ActionTask(action);
			return Then(task);
		}

		#endregion
	}
}