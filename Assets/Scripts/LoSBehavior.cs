﻿using UnityEngine;
using System.Collections;

namespace gc{
	public class LoSBehavior : MonoBehaviour {

		public static LoSBehavior Instance { get; private set; }

		void Awake() {
			Instance = this;
		}

		void Start()
		{
			EventManager.Instance.Register<CompletedPassEvent>(OnCompletedPass);

		}

		public float YPosition()
		{
			return transform.position.y;
		}

		void MoveToNewPosition(float newY)
		{
			transform.position = new Vector2(transform.position.x, newY);
		}

		void OnCompletedPass(Event e)
		{
			CompletedPassEvent cPE = (CompletedPassEvent)e;
			//only move if the pass was forwards
			if(cPE.yPosition>transform.position.y){
				MoveToNewPosition(cPE.yPosition);
			}
		}

		void OnDestroy()
		{
			EventManager.Instance.Unregister<CompletedPassEvent>(OnCompletedPass);
		}
	}
}
