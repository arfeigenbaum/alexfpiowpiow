﻿using UnityEngine;
using System.Collections;

namespace gc{

	public class DashAtBall : EnemyMovement {
		//dash timer is the range that the timer can be between
		Vector2 dashTimerStart = new Vector2(1.5f, 3f);

		float dashTimer;
		float dashForce = 130f;

		protected override void Start()
		{
			drag = 5f;
			base.Start();
			//start timer randomly in the dash range
			dashTimer=Random.Range(dashTimerStart.x, dashTimerStart.y);
		}

		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			//if dashtimer is positive subtract time
			if(dashTimer>0f){
				dashTimer-=Time.deltaTime;
			}
			//if dash timer is under 0 then dash toward the ball
			else{
				DashTowardBall(dashForce);
				dashTimer=Random.Range(dashTimerStart.x, dashTimerStart.y);
			}

		}

	}
}