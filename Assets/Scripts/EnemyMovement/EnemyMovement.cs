﻿
using UnityEngine;

namespace gc {

    public class EnemyMovement : MovementController {

		public float MaxSpeed;
		protected float drag = 0f;

		protected virtual void Start()
		{
			if(drag!=0f){
				body.drag = drag;
			}
		}

		protected override void Update()
		{
			base.Update();
		}

        protected override void FixedUpdate()
        {
			base.FixedUpdate();
			//break if there is no ball
			if(Ball.Instance==null){
				return;
			}
        }

		protected void DashTowardBall(float force)
		{
			Vector2 directionToBall = DirectionTowardPosition(Ball.Instance.transform.position);
			body.AddForce(force*directionToBall, ForceMode2D.Impulse);
			FaceMoveDirection();
		}

		Vector2 DirectionTowardPosition(Vector2 target)
		{
			Vector2 targetPosition = target;
			Vector2 myPosition = transform.position;
			Vector2 offset = targetPosition-myPosition;
			return offset.normalized;

		}
		//check if a transform is within range distance of this enemy
		protected bool TargetIsInRange(Transform target, float range)
		{
			if(Vector3.Distance(target.position, transform.position)<range){
				return true;
			}
			return false;
		}
		//move toward a specific spot
		protected void MoveTowardSpot(Vector2 spot)
		{
			//10 is an arbitrary distance close enough to the position
			if(Vector2.Distance(spot, transform.position)>10f){
				
				body.AddForce(MaxSpeed*DirectionTowardPosition(spot));
				float currentSpeed = body.velocity.magnitude;
				if (currentSpeed > MaxSpeed) {
					body.velocity = Vector2.ClampMagnitude(body.velocity, MaxSpeed);
				}
				FaceMoveDirection();
			}
			else{
				ZeroVelocity();
			}
			
		}

		protected void ZeroVelocity()
		{
			body.velocity=Vector2.zero;
			body.angularVelocity=0f;
		}

		protected void ChaseTargetReynolds(Transform target)
		{
			
			if (target == null) {
				return;
			}

			// get the player's position and where it is in relation to this enemy
			Vector2 directionToPlayer = DirectionTowardPosition(target.position);

			// Determine the steering force (this is using Craig Reynolds' classic approach. You can find out more here: http://www.red3d.com/cwr/steer/gdc99/)
			Vector2 desiredVelocity = directionToPlayer * MaxSpeed;
			Vector2 steeringForce = desiredVelocity - body.velocity;
			steeringForce = Vector2.ClampMagnitude(steeringForce, MaxSpeed);
			body.AddForce(steeringForce);
			float currentSpeed = body.velocity.magnitude;
			if (currentSpeed > MaxSpeed) {
				body.velocity = Vector2.ClampMagnitude(body.velocity, MaxSpeed);
			}

			// make the body face in the direction of movement
			FaceMoveDirection();
		}

		protected virtual void OnDestroy()
		{
			EventManager.Instance.Fire(new DestroyedEnemyEvent(this));
		}

		void FaceMoveDirection()
		{
			float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
			body.MoveRotation(angle);	
		}
		Bounds LevelBounds()
		{
			Camera cam = Camera.main;
			float height = 2f * cam.orthographicSize;
			float width = height * cam.aspect;
			Vector3 center = new Vector3(width/2f, height/2f, 0f);
			return new Bounds(center, center);
		}
		//returns a new spot
		public Vector3 RandomSpotAtLeastSomeDistanceAway(float minMoveDistance)
		{
			Vector3 newPos = transform.position;
			Bounds levelBounds = LevelBounds();
			//get a new point until you are over the minimum distance
			while(Vector3.Distance(newPos, transform.position) < minMoveDistance){
				float x = Random.Range(levelBounds.min.x, levelBounds.max.x);
				float y = Random.Range(levelBounds.min.y, levelBounds.max.y);
				newPos = new Vector3(x, y, 0f);
			}
			return newPos;
		}
    }

}
