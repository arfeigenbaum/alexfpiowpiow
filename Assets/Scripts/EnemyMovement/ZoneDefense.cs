﻿using UnityEngine;
using System;
using System.Collections;

namespace gc{
	
	public class ZoneDefense : EnemyMovement 
	{
		
		//choose a circular zone around where you are
		float zoneRange= 25f;
		float chaseRange = 40f;
		//choose a zone where you will stop chasing and go back to zone

		//Think about using a delegate system here?
		bool isChasing;
		//area this enemy starts in
		//returns there when player is too far away
		Vector2 homeSpot;
		//if a player moves into that zone then acquire and chase it
		TaskManager tm;
		protected override void Start(){
			base.Start();
			MaxSpeed = UnityEngine.Random.Range(30,100);
			homeSpot = transform.position;
		}

		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			//if you are chasing the target
			if(isChasing){
				//check if target is still in your chase range
				if(IsTargetInRange(chaseRange)){
					ChaseTargetReynolds(Ball.Instance.transform);
				}
				//otehrwise stop chasing and set velocity to 0
				else{
					ReturnHome();
				}
			}
			//if you are not chasing
			else{
				//if it's in your zone range start chasing the ball
				if(TargetIsInRange(Ball.Instance.transform, zoneRange)){
					isChasing=true;
					ChaseTargetReynolds(Ball.Instance.transform);
				}
				//otherwise move back home
				else{
					ReturnHome();
				}
			}
		}

		void ReturnHome()
		{
			isChasing=false;
			MoveTowardSpot(homeSpot);
		}

		bool IsTargetInRange(float range)
		{
			return TargetIsInRange(Ball.Instance.transform, range);
		}
	}
}