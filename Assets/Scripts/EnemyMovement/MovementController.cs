﻿using UnityEngine;

namespace gc {

    public abstract class MovementController : MonoBehaviour {

		protected Rigidbody2D body;

        protected virtual void Awake () {
            body = GetComponent<Rigidbody2D>();
            if (body == null) {
                throw new MissingComponentException("MovementController added to game object that has no Rigidbody2D");
            }
        }

        protected virtual void FixedUpdate()
		{
			if(Game.Instance.isPaused)
			{
				return;
			}
		}

		protected virtual void Update(){

			if(Game.Instance.isPaused)
			{
				return;
			}
		}
    }
}
