﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StatePattern;
using gc;

public class StateDefense : EnemyMovement {
	private FSM<StateDefense> _states;
	private Transform target;
	void Start()
	{
		//make a new state machine with this as the context
		target = Ball.Instance.transform;
		_states  = new FSM<StateDefense>(this);
		_states.TransitionTo<Seeking>();
	}

	protected override void Update()
	{
		base.Update();
		_states.Update();
	}

	protected override void OnDestroy()
	{
		_states.TransitionTo<Destroy>();
		base.OnDestroy();
		//switch to a destroying state, to clean everything up
	}

	private class Seeking : FSM<StateDefense>.State
	{
		protected float seekingRange = 35f;

		public override void OnEnter(FSM<StateDefense>.State previousState)
		{
			Parent._context.target = Ball.Instance.transform;
			Parent._context.MaxSpeed = 20f;
		}
		public override void Update()
		{
			if(Parent._context.TargetIsInRange(Parent._context.target, seekingRange))
			{
				TransitionTo<AttackPreparation>();
			}
			else{
				Parent._context.ChaseTargetReynolds(Ball.Instance.transform);
			}
		}
	}
	private class AttackPreparation : FSM<StateDefense>.State
	{
		Coroutine pulseCoroutine;

		public override void OnEnter(FSM<StateDefense>.State previousState)
		{
			if(previousState.GetType() == typeof(Seeking))
			{
				StartPulsing();
				Parent._context.ZeroVelocity();
			}
			EventManager.Instance.Register<ScoredGoalEvent>(OnPlayerScored);
		}
			
		public override void Update()
		{
			
		}

		void OnPlayerScored(gc.Event e)
		{
			StopPulsing();
			TransitionTo<Fleeing>();
		}

		public override void OnExit(FSM<StateDefense>.State nextState)
		{
			EventManager.Instance.Unregister<ScoredGoalEvent>(OnPlayerScored);
		}

		void StartPulsing()
		{
			if(pulseCoroutine!=null)
			{
				Parent._context.StopCoroutine(pulseCoroutine);
			}
			pulseCoroutine = Parent._context.StartCoroutine(StartPulsing(5));
		}

		void StopPulsing()
		{
			if(pulseCoroutine!=null)
			{
				//move to parent
				Parent._context.StopCoroutine(pulseCoroutine);
				Parent._context.transform.localScale = Vector3.one;
			}
		}

		IEnumerator StartPulsing(int pulses)
		{
			Vector3 bigScale = new Vector3(2,2,2);
			Vector3 smallScale = new Vector3(1,1,1);
			float pulseTime = .2f;
			Parent._context.transform.localScale = smallScale;
			for(int i=0; i<pulses; i++)
			{
				for(float t=0; t<1.0f; t+=Time.deltaTime/pulseTime)
				{
					Parent._context.transform.localScale=Vector3.Lerp(smallScale,bigScale,t);
					yield return null;
				}
				yield return new WaitForSeconds(.3f);
				for(float t=0; t<1.0f; t+=Time.deltaTime/pulseTime)
				{
					Parent._context.transform.localScale=Vector3.Lerp(bigScale,smallScale,t);
					yield return null;
				}
			}
			Parent._context.transform.localScale = smallScale;
			if(Parent._context._states.CurrentState.GetType() == typeof(AttackPreparation))
			{
				TransitionTo<Attack>();
			}
			yield break;
		}
	}

	private class Attack : FSM<StateDefense>.State
	{
		float timeInState = 0f;
		float maxTimeInState = 2.25f;
		float dashForce = 100f;

		public override void OnEnter(FSM<StateDefense>.State previousState)
		{
			timeInState = 0f;
			Parent._context.DashTowardBall(dashForce);
		}
		public override void Update()
		{
			timeInState+=Time.deltaTime;
			if(timeInState>=maxTimeInState)
			{
				TransitionTo<Fleeing>();
			}
		}
	}


	private class Fleeing : FSM<StateDefense>.State
	{
		Vector3 fleeTarget;
		float fleeTime = 0f;
		float timeToFlee = 2.25f;
		float minFleeDistance = 30f;
		public override void OnEnter(FSM<StateDefense>.State previousState)
		{
			fleeTime = 0f;
			fleeTarget = Parent._context.RandomSpotAtLeastSomeDistanceAway(minFleeDistance);
		}
		public override void Update()
		{
			fleeTime+=Time.deltaTime;
			Parent._context.MoveTowardSpot(fleeTarget);
			if(fleeTime>=timeToFlee)
			{
				TransitionTo<Seeking>();
			}
		}
	}

	private class Destroy : FSM<StateDefense>.State
	{
		//just clean up for exit
	}
}
