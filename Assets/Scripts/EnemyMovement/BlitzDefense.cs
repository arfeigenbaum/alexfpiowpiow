﻿using UnityEngine;
using System.Collections;

namespace gc{

	public class BlitzDefense : EnemyMovement {
		
		Vector2 maxSpeedRange = new Vector2(40f, 60f);
		Transform targetPlayer;
		float maxSpeedBoost = 100f;

		protected override void Start()
		{
			MaxSpeed = Random.Range(maxSpeedRange.x, maxSpeedRange.y);
			base.Start();
			//start timer randomly in the dash range
			ChooseTarget();
			//change target based on the ball changing hands
			EventManager.Instance.Register<CompletedPassEvent>(OnCompletedPass);
		}
			

		void OnCompletedPass(Event e)
		{
			targetPlayer = ((CompletedPassEvent)e).receiver.transform;
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
			EventManager.Instance.Unregister<CompletedPassEvent>(OnCompletedPass);
		}

		//choose one player to blitz
		void ChooseTarget()
		{
			GameObject[] players = GameObject.FindGameObjectsWithTag("PLAYER");
			if(players.Length>0){
				targetPlayer=players[Random.Range(0,players.Length)].transform;
			}
		}
		//always move toward the player
		protected override void FixedUpdate()
		{
			base.FixedUpdate();
			if(targetPlayer!=null){
				ChaseTargetReynolds(targetPlayer);
			}
			else{
				ChooseTarget();
			}
		}
	}
}