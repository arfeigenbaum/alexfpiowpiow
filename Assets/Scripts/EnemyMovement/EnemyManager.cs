﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace gc{
	public class EnemyManager : MonoBehaviour {

		public static EnemyManager Instance { get; private set; }

		void Awake() {
			if(Instance == null){
				Instance = this;
			}
			else{
				Destroy(this);
			}
		}


		enum GameState{
			PauseBetweenWaves,
			InWave,
			ConstantSpawn
		}

		GameState gameState = GameState.PauseBetweenWaves;

		float waveTimer;
		public float pauseTimerStart = 2f;
		float pauseTimer = 1f;
		int waveNumber = 0;
		//enemy scripts
		List<Enemy> allLivingEnemies = new List<Enemy>();

		List<GameObject> enemyPrefabs = new List<GameObject>();
		List<Enemy> enemiesAwaitingDestruction = new List<Enemy>();
//
//		public enum EnemyType
//		{
//			Zone,
//			Blitz,
//			Dash,
//			Boss
//		};

		void Start()
		{
			//load all the different enemy prefabs
			foreach(GameObject g in Resources.LoadAll("Prefabs/Enemies")){
				enemyPrefabs.Add(g);
			}
			//don't do this yet
		}
			
		void Update()
		{
			if(gameState==GameState.InWave){
				InWave();
			}
			else if (gameState == GameState.PauseBetweenWaves){
				PauseBetweenWaves();
			}
			else if (gameState == GameState.ConstantSpawn)
			{
				ConstantSpawn();
			}
			//destroy all enemies under the camera
			for (int i=allLivingEnemies.Count-1; i>=0; i--)
			{
				if(IsEnemyUnderCamera(allLivingEnemies[i]))
				{
					DestroyEnemy(allLivingEnemies[i]);
				}
			}
			for(int i=0; i<enemiesAwaitingDestruction.Count; i++){
				Destroy(enemiesAwaitingDestruction[i].gameObject);
			}
			enemiesAwaitingDestruction.Clear();
		}

		void ConstantSpawn()
		{
			//this is run through the wave manager instead
		}

		void InWave()
		{
			waveTimer-=Time.deltaTime;
			if(waveTimer<0){
				EndWave();
			}
			//if you're out of enemies, also end the wave
			else if (allLivingEnemies.Count ==0)
			{
				EndWave();
			}
		}

		void PauseBetweenWaves()
		{
			pauseTimer-=Time.deltaTime;
			if(pauseTimer<0){
				EventManager.Instance.Fire(new NeedsNewWaveEvent());
				pauseTimer=pauseTimerStart;
			}
		}

		bool IsEnemyUnderCamera(Enemy e)
		{
			if(e.transform.position.y<CameraMovement.Instance.CamBoundsAtCurrentPosition().min.y)
			{
				return true;
			}
			return false;
		}

		//called in enemy's destroy method
		public void DestroyEnemy(Enemy e)
		{
			enemiesAwaitingDestruction.Add(e);
			allLivingEnemies.Remove(e);
		}

		public void DestroyAllEnemies()
		{
			for(int i=0; i<allLivingEnemies.Count; i++){
				enemiesAwaitingDestruction.Add(allLivingEnemies[i]);
			}
			allLivingEnemies.Clear();
		}

		public void SpawnWave(WaveManager.WaveOfEnemies wave)
		{
			for(int i=0; i<wave.enemiesThisWave.Count; i++)
			{
				SpawnEnemy(wave.enemiesThisWave[i].myType, wave.enemiesThisWave[i].spawnLocation);
			}
			waveTimer = wave.waveTime;
			gameState = GameState.InWave;
		}
			
		public void SpawnEnemyRandom(Vector3 position)
		{
			EnemyType type = (EnemyType)Random.Range(0, 3);
			GameObject newEnemy = Instantiate(enemyPrefabs[PointInEnemyList(type)], position, Quaternion.identity) as GameObject;
			allLivingEnemies.Add(newEnemy.GetComponent<Enemy>());
		}

		void SpawnEnemy(EnemyType type, Vector3 position)
		{
			GameObject newEnemy = Instantiate(enemyPrefabs[PointInEnemyList(type)], position, Quaternion.identity) as GameObject;
			allLivingEnemies.Add(newEnemy.GetComponent<Enemy>());
		}
		//turns a type into a prefab
		int PointInEnemyList(EnemyType type)
		{
			for(int i=0; i<enemyPrefabs.Count; i++){
				if(enemyPrefabs[i].name.Contains(type.ToString())){
					return i;
				}
			}
			Debug.LogError("CANT FIND ENEMY: " + type.ToString());
			return 0;
		}
			
		void EndWave()
		{
			//destroy all enemies

			//pause for an amount of time
			pauseTimer=pauseTimerStart;
			gameState=GameState.PauseBetweenWaves;
			pauseTimer = pauseTimerStart;
			DestroyAllEnemies();
			waveNumber++;
		}

		public void ResetGame(float time=0f){
			Invoke("Reset", time);
		}

		void Reset(){
			DestroyAllEnemies();
			Application.LoadLevel(Application.loadedLevel);
		}

	}

}
