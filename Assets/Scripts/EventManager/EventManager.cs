﻿using System;
using System.Collections.Generic;

namespace gc{

	public abstract class Event {
		public delegate void Handler(Event e);
	}
	// and subclasses will add the details
	public class PlayerDiedEvent : Event
	{
		public readonly int player;
		public PlayerDiedEvent(int player) {
			this.player = player;
		}
	}

	public class CompletedPassEvent: Event
	{
		public readonly PlayerMovement receiver;
		public readonly float yPosition;
		public CompletedPassEvent (PlayerMovement receiver, float yPosition)
		{
			this.receiver=receiver;
			this.yPosition = yPosition;
		}
	}
		
	public class ThrowPassEvent : Event{
		public readonly PlayerMovement passer;
		public ThrowPassEvent (PlayerMovement passer)
		{
			this.passer = passer;
		}
	}
	//might not need any info
	public class NeedsNewWaveEvent: Event
	{
		
	}

	public class WaveEndedEvent: Event
	{
		
	}

	public class DestroyedEnemyEvent : Event{
		public readonly EnemyMovement eM;
		public DestroyedEnemyEvent(EnemyMovement eM)
		{
			this.eM = eM;
		}
	}

	public class ScoredGoalEvent : Event
	{
		
	}

	public class EventManager {

		// Just some singleton boiler plate
		static private EventManager instance;
		static public EventManager Instance { get {
				if (instance == null) {
					instance = new EventManager();
				}
				return instance;
			} }

		// Here is the core of the system: a dictionary that maps Type (specifically Event types in this case)
		// to Event.Handlers
		private Dictionary<Type, Event.Handler> registeredHandlers = new Dictionary<Type, Event.Handler>();

		// This is where you can add handlers for events. We use generics for 2 reasons:
		// 1. Passing around Type objects can be tedious and verbose
		// 2. Using generics allows us to add a little type safety, by getting
		// the compiler to ensure we're registering for an Event type and not
		// some other random type
		public void Register<T>(Event.Handler handler) where T : Event {
			// NOTE: This method does not check to see if a handler was registered twice
			Type type = typeof(T);
			if (registeredHandlers.ContainsKey(type)) {
				registeredHandlers[type] += handler;
			} else {
				registeredHandlers[type] = handler;
			}
		}

		public void Unregister<T>(Event.Handler handler) where T : Event {
			Type type = typeof(T);
			Event.Handler handlers;
			if (registeredHandlers.TryGetValue(type, out handlers)) {
				handlers -= handler;
				if (handlers == null) {
					registeredHandlers.Remove(type);	
				} else {
					registeredHandlers[type] = handlers;
				} 
			}
		}

		public void Fire(Event e) {
			Type type = e.GetType();
			Event.Handler handlers;
			if (registeredHandlers.TryGetValue(type, out handlers)) {
				handlers(e);
			}
		}
	}
}