﻿using UnityEngine;
using UnityEngine.UI;
namespace gc {

	public class Game : MonoBehaviour {
		public static Game Instance { get; private set; }

		void Awake() {
			Instance = this;
		}

		public int MinNumEnemies = 6;
		int playerGutter = 10;
		string[] allEnemies;

		float score = 0f;
		float startingPoint = 0f;
		public bool isPaused = false;
		GameObject pausePanel;
		Text scoreText;
		bool gameOver=false;

		public void TogglePause()
		{
			if(isPaused)
			{
				isPaused=false;
				pausePanel.SetActive(false);
				Time.timeScale=1.0f;
			}
			else{
				isPaused=true;
				pausePanel.SetActive(true);
				Time.timeScale=0f;
			}
			pausePanel.GetComponentInChildren<Text>().fontSize = 100;
		}

		public void EndGame()
		{
			gameOver = true	;
			pausePanel.SetActive(true);
			Time.timeScale=0f;
			isPaused=true;
			pausePanel.GetComponentInChildren<Text>().fontSize = 40;
			pausePanel.GetComponentInChildren<Text>().text = "Game Over. Click to Play Again";
		}

		void Reset()
		{
			Time.timeScale=1f;
			Application.LoadLevel(Application.loadedLevel);
		}

		void Start()
		{
			SpawnPlayers();
			SpawnBall();
			allEnemies = new string[4]
			{
				"Prefabs/Enemies/BlitzEnemy",
				"Prefabs/Enemies/ZoneEnemy",
				"Prefabs/Enemies/DashEnemy",
				"Prefabs/Enemies/StateDefense"
			};
			EventManager.Instance.Register<CompletedPassEvent>(OnCompletedPass);
			pausePanel = GameObject.Find("PausePanel");
			scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
			pausePanel.SetActive(false);
			startingPoint = LoSBehavior.Instance.YPosition();
			Debug.Log("START");
		}

		void OnEnable()
		{
			Debug.Log("ON ENABLE");
			pausePanel = GameObject.Find("PausePanel");
			scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
		}

		//add a score everytime a pass in completed
		void OnCompletedPass(Event e)
		{
			CompletedPassEvent cPE = (CompletedPassEvent)e;
			float pos = cPE.yPosition;
			int newScore = ScoreFromPosition(pos);
			if(scoreText == null)
			{
				scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
			}
			scoreText.text = ""+newScore+" Yards";
		}

		int ScoreFromPosition(float pos)
		{
			pos = pos - startingPoint;
			float multipler = .1f;
			return Mathf.FloorToInt(pos*multipler);
		}

		void SpawnPlayers()
		{
			Spawner.Spawn("Prefabs/PlayerNew", new Vector3(Config.WorldWidth-playerGutter, 3));
			Spawner.Spawn("Prefabs/PlayerNew", new Vector3(playerGutter, 3));
		}

		void SpawnBall()
		{
			Spawner.Spawn("Prefabs/Football", new Vector3(Config.WorldWidth-playerGutter, 3));
		}

		void SpawnStartingEnemies()
		{
			SpawnEnemy(allEnemies[0]);
			SpawnEnemy(allEnemies[1]);
			SpawnEnemy(allEnemies[2]);
		}

		void Update () 
		{
			if(gameOver)
			{
				if(Input.GetMouseButtonDown(0))
				{
					Reset();
				}
			}
			else if(Input.GetKeyDown(KeyCode.Escape))
			{
				TogglePause();
			}

		}
		//get a random enemy name
		string RandomEnemyName(){
			int rand = Random.Range(0,allEnemies.Length);
			return allEnemies[rand];
		}
		void SpawnEnemy(string enemyName){
			Spawner.Spawn(enemyName, RandomWorldPosition(3));
		}
			
		private Vector3 RandomWorldPosition(int gutter) {
			return new Vector3(Random.Range(gutter, Config.WorldWidth - gutter), Random.Range(gutter, Config.WorldHeight - gutter), 0); 
		}

	}

}

