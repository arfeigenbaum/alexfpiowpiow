﻿using UnityEngine;
using StatePattern;
using UnityEngine.UI;

namespace gc {

	public class PlayerMovement : MovementController {

		public float offBallSpeed;
		public float hasBallSpeed;
		public float throwForce;
		public float chargeTime;
		public float minThrowForce;

		Transform otherPlayer;

		[HideInInspector]
		public PlayerMovement otherPlayerMovement;

		public FSM<PlayerMovement> _states;

		[HideInInspector]
		public Image powerImage;

		protected override void Awake()
		{
			base.Awake();
			_states  = new FSM<PlayerMovement>(this);
			_states.TransitionTo<ChasingBall>();
			powerImage = GetComponentInChildren<Image>();
			powerImage.enabled=false;
		}

		void Start()
		{
			FindOtherPlayer();
			otherPlayerMovement=otherPlayer.GetComponent<PlayerMovement>();
		}

        override protected void FixedUpdate() 
		{
			base.FixedUpdate();
		}

		override protected void Update()
		{
			//pause if paused
			base.Update();
			_states.Update();
		}

		public Vector2 ForceDirection()
		{
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 shipPosition = gameObject.transform.position;
			Vector3 offsetToMouse = mousePosition - shipPosition;
			return offsetToMouse.normalized;
		}

		protected Vector2 DirectionToOtherPlayer()
		{
			if(otherPlayer==null)
			{
				FindOtherPlayer();
			}

			Vector3 myPosition = gameObject.transform.position;
			Vector3 otherPosition = otherPlayer.position ;
			Vector3 direction = otherPosition-myPosition;
			return direction.normalized;
		}

		void FindOtherPlayer()
		{

			GameObject[] players = GameObject.FindGameObjectsWithTag("PLAYER");
			if(players.Length!=2){
				Debug.LogError("TOO MANY PLAYERS???");
			}
			for(int i =0; i<players.Length; i++){
				if(players[i]!=this.gameObject)
				{
					otherPlayer = players[i].transform;
					otherPlayerMovement = otherPlayer.GetComponent<PlayerMovement>();
				}
			}
		}

		public void HitByEnemy()
		{
			if(_states.CurrentState.GetType() == typeof(HasBall) || _states.CurrentState.GetType() == typeof(ChargingThrow)){
				Ball.Instance.EndGame();
			}
			else{
				//do nothing
			}
		}

		public void GetBall()
		{
			Ball.Instance.GetCaught(transform);
			Ball.Instance.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			//don't call the completed pass if this is the first time picking up the ball to star the game
			if(Ball.Instance.hasBeenThrown){
				EventManager.Instance.Fire(new CompletedPassEvent(this, transform.position.y));
			}
			_states.TransitionTo<HasBall>();
			FindOtherPlayer();
			otherPlayerMovement._states.TransitionTo<MovingWithoutTheBall>();
		}

		bool CanPickUpBall()
		{
			if(_states.CurrentState.GetType() == typeof(ChasingBall)
				|| _states.CurrentState.GetType() == typeof(ControllingWithoutBall))
			{
				return true;
			}
			return false;
		}

		void OnCollisionEnter2D(Collision2D col)
		{
			if(col.gameObject.CompareTag("BALL"))
			{
				if(CanPickUpBall()) 
				{
					GetBall();
				}
			}
		}


		Vector2 DirectionTowardPosition(Vector2 target)
		{
			Vector2 targetPosition = target;
			Vector2 myPosition = transform.position;
			Vector2 offset = targetPosition-myPosition;
			return offset.normalized;

		}

		void FaceMoveDirection()
		{
			float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
			body.MoveRotation(angle);	
		}

		public void MoveTowardPoint(Vector2 target, float speed)
		{
			float snapDist = 10f;
			if(Vector2.Distance(target, transform.position)>snapDist){

				body.AddForce(speed*DirectionTowardPosition(target));
				float currentSpeed = body.velocity.magnitude;
				if (currentSpeed > speed) {
					body.velocity = Vector2.ClampMagnitude(body.velocity, speed);
				}
				FaceMoveDirection();
			}
			else
			{
				//transform.position = target;
			}
		}

		public void ZeroVelocity()
		{
			body.velocity=Vector2.zero;
			body.angularVelocity=0f;
		}

		public void MoveWithMouse(float maxSpeed)
		{
			// Clear the velocity every frame so the player's ship responds instantly to changes in direction
			body.velocity = Vector2.zero;
			body.angularVelocity = 0;

			// determine the positions of the mouse and player
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3 shipPosition = transform.position;
			Vector3 offsetToMouse = mousePosition - shipPosition;

			// Don't bother updating the forces if the player is very close to the mouse
			float distanceToMouse = offsetToMouse.magnitude;
			if (distanceToMouse > Mathf.Epsilon) 
			{

				// scale the speed so that the force gets smaller when the ship gets close to the mouse
				// NOTE: The scaling the distance by the inverse of the time step has to do with how Box2D handles forces vs. impulses.
				float speed = Mathf.Min(maxSpeed, (distanceToMouse / Time.fixedDeltaTime));

				// calculate the direction of the force required to move the player towards the mouse
				Vector2 forceDirection = offsetToMouse.normalized;
				Vector2 force = forceDirection * speed;

				// apply the force as an impulse (impulses are easier if you're constantly clearing the forces like we do here)
				body.AddForce(force, ForceMode2D.Impulse);
			}
			transform.position = LockedYPosition(transform.position);
		}

		Vector2 LockedYPosition(Vector2 position)
		{
			if(position.y>LoSBehavior.Instance.transform.position.y)
			{
				return new Vector2(position.x, LoSBehavior.Instance.transform.position.y);
			}
			return position;
		}
    }


	public class HasBall : FSM<PlayerMovement>.State
	{
		Rigidbody2D body;
		float maxSpeed;
		Transform trans;
		public override void Init ()
		{
			base.Init ();
			body = Parent._context.GetComponent<Rigidbody2D>();
			maxSpeed = Parent._context.hasBallSpeed;
			trans = Parent._context.transform;
		}
		public override void Update ()
		{
			base.Update ();

			if(Input.GetMouseButtonDown(0))
			{
				//transition to charging throw
				TransitionTo<ChargingThrow>();
			}
			else
			{
				Parent._context.MoveWithMouse(maxSpeed);
			}
		}
	}

	public class ChargingThrow: FSM<PlayerMovement>.State
	{
		float timeInMode;
		float maxChargeTime;
		float maxThrowForce;
		float minThrowForce;

		public override void Init ()
		{
			base.Init ();
			maxThrowForce = Parent._context.throwForce;
			maxChargeTime = Parent._context.chargeTime;
			minThrowForce = Parent._context.minThrowForce;
		}

		public override void OnEnter (FSM<PlayerMovement>.State previousState)
		{
			base.OnEnter (previousState);
			Parent._context.ZeroVelocity();
			Parent._context.powerImage.enabled=true;
			timeInMode=0f;
			SetPowerMeter(0f);
		}

		public override void Update ()
		{
			base.Update ();
			//throw when the mouse comes up
			if(Input.GetMouseButtonUp(0))
			{
				Throw();
			}
			else
			{
				timeInMode+=Time.deltaTime;
				SetPowerMeter(timeInMode/maxChargeTime);
				if(timeInMode>maxChargeTime)
				{
					timeInMode = maxChargeTime;
				}
			}
		}

		void Throw()
		{
			float force = timeInMode/maxChargeTime*maxThrowForce;
			if(force<minThrowForce)
			{
				force = minThrowForce;
			}
			Ball.Instance.GetThrown(force, Parent._context.ForceDirection());
			//transition to controlling without ball
			TransitionTo<ControllingWithoutBall>();
			Parent._context.otherPlayerMovement._states.TransitionTo<ChasingBall>();

		}

		void SetPowerMeter(float amt)
		{
			Parent._context.powerImage.fillAmount = amt;

		}
		public override void OnExit (FSM<PlayerMovement>.State nextState)
		{
			base.OnExit (nextState);
			Parent._context.powerImage.enabled=false;
		}
	}

	public class ControllingWithoutBall : FSM<PlayerMovement>.State
	{
		float timer = 0f;
		float maxTime = .75f;

		public override void Init ()
		{
			base.Init ();
		}
		public override void OnEnter (FSM<PlayerMovement>.State nextState)
		{
			base.OnEnter (nextState);
			timer=0f;
		}
		public override void Update ()
		{
			base.Update ();
			if(timer<maxTime)
			{
				timer+=Time.deltaTime;
				//do nothing
			}
			else
			{
				Parent._context.MoveWithMouse(Parent._context.offBallSpeed);
			}
		}
	}

	public class ChasingBall : FSM<PlayerMovement>.State
	{
		float timer = 0f;
		float maxTime = .25f;
		Vector2 target;

		public override void OnEnter (FSM<PlayerMovement>.State previousState)
		{
			base.OnEnter (previousState);
			timer = 0f;
			target = Target();
		}

		public override void Update ()
		{
			base.Update ();
			if(timer<maxTime)
			{
				timer+=Time.deltaTime;
				//do nothing
			}
			else
			{
				Parent._context.MoveTowardPoint(Target(), Parent._context.offBallSpeed);
			}
		}

		Vector2 Target()
		{
			if(Ball.Instance!=null){
				return (Vector2)Ball.Instance.transform.position;
			}
			else{
				return Parent._context.transform.position;
			}
		}
	}

	public class MovingWithoutTheBall : FSM<PlayerMovement>.State
	{

		enum MovingTo{
			Top,
			Right,
			Left,
			JustEntered
		}
		float minDist = 10f;
		MovingTo mT;
		Vector2 target;
		public override void OnEnter (FSM<PlayerMovement>.State previousState)
		{
			//zero rigid body
			Parent._context.ZeroVelocity();
			mT =MovingTo.JustEntered;
			ChangeInternalState(MovingTo.Top);
		}

		void ChangeInternalState(MovingTo moveTo)
		{
			if(moveTo!=mT)
			{
				mT=moveTo;
				target = Target();
			}
		}

		Vector2 Target()
		{
			if(mT == MovingTo.Top)
			{
				float x = CameraMovement.Instance.CamXAsPercent(.2f);
				float y = CameraMovement.Instance.CameraYAsPercent(.8f);
				return new Vector2(x,y);
			}
			else if (mT == MovingTo.Left)
			{
				float x = CameraMovement.Instance.CamXAsPercent(.2f);
				float y = CameraMovement.Instance.CameraYAsPercent(.8f);
				return new Vector2(x,y);
			}
			else if (mT == MovingTo.Right)
			{
				float x = CameraMovement.Instance.CamXAsPercent(.8f);
				float y = CameraMovement.Instance.CameraYAsPercent(.8f);
				return new Vector2(x,y);
			}
			//should be unreachable
			return Vector2.zero;
		}

		bool ReachedTarget()
		{
			if(Vector2.Distance(Parent._context.transform.position, target) < minDist)
			{
				return true;
			}
			return false;
		}

		void ChangeDirection()
		{
			if(mT == MovingTo.Left)
			{
				ChangeInternalState(MovingTo.Right);
			}
			else if (mT == MovingTo.Right)
			{
				ChangeInternalState(MovingTo.Left);
			}
			else if (mT == MovingTo.Top)
			{
				ChangeInternalState(MovingTo.Left);
			}
		}

		public override void Update ()
		{
			base.Update ();
			target = Target();
			Parent._context.MoveTowardPoint(target, Parent._context.offBallSpeed);
			if(ReachedTarget())
			{
				ChangeDirection();
			}
		}
	}
}
