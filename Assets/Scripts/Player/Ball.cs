﻿using UnityEngine;
using System.Collections;
namespace gc{
	
	public class Ball : MovementController {
		public static Ball Instance { get; private set; }

		public enum BallState{
			held,
			inAir
		}

		protected override void Awake() {
			base.Awake();
			Instance = this;
			myCol = GetComponent<Collider2D>();
			myCol.enabled=false;
			Invoke("EnableCol", .125f);
		}

		protected void EnableCol()
		{
			myCol.enabled=true;
		}

		Transform parent = null;
		Collider2D myCol;

		public BallState ballState = BallState.inAir;

		public bool hasBeenThrown = false;

		//parent to play when they pick it up
		//called through player scripts
		public void GetCaught(Transform trans)
		{
			parent = trans;
			ballState = BallState.held;
			myCol.enabled=false;
		}

		protected override void FixedUpdate()
		{
			//move with parent while held
			//otherwise you move with rb force
			//force is added through players
			if(parent !=null){
				transform.position = parent.position;
			}
		}

		public Vector2 Velocity()
		{
			return body.velocity;
		}

		public void GetThrown(float force, Vector2 direction)
		{
			GetCaught(null);
			body.velocity = Vector2.zero;
			body.AddForce(direction*force, ForceMode2D.Impulse);
			ballState = BallState.inAir;
			Invoke("EnableCol", .25f);
			hasBeenThrown=true;
		}

		public void EndGame()
		{
			GameObject[] players = GameObject.FindGameObjectsWithTag("PLAYER");
			for(int i=0; i<players.Length; i++){
				Destroy(players[i]);
			}
			Game.Instance.EndGame();
			//Destroy(this.gameObject);
		}

		void OnCollisionEnter2D(Collision2D col)
		{
			//end game if the ball hits an enemy
			if(col.gameObject.tag == "ENEMY")
			{
				EndGame();
			}
		}

	}
}
