﻿using UnityEngine;

namespace gc {

	public class Player : MonoBehaviour {

		public static Player Instance { get; private set; }

		public int Health = 100;

		void Awake() {
			Instance = this;
		}

		void Update() {
			if (Health <= 0) {
				Destroy(gameObject);
			}
		}

		void ApplyDamage(int damage) {
			Health -= damage;
			if (Health <= 0) {
				AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/player_destroyed");
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			}
		}

		void OnDestroy() {
			Instance = null;
		}

	}

}


